.. _common_steps_explain:

common\_steps
=============

To view source code :ref:`common_steps_source`

.. _mouse_click_example:

Mouse button click
------------------

Decorators::

    {m_btn} click "{name}" "{role_name}"
    {m_btn} click "{name}" "{role_name}" in "{root}"
    {m_btn} click "{name}" "{role_name}" that is "{attr}"
    {m_btn} click "{name}" "{role_name}" that is "{attr}" in "{root}"
    {m_btn} click "{name}" "{role_name}" with description "{descr}"
    {m_btn} click "{name}" "{role_name}" with description "{descr}" in "{root}"
    {m_btn} click "{name}" "{role_name}" with description "{descr}" that is "{attr}"
    {m_btn} click "{name}" "{role_name}" with description "{descr}" that is "{attr}" in "{root}"

Decorators can be parameterized with the above syntax.
The requirement is having the variable in **{}** and having the same variable in the function definition::

    @step('Decorator text and {this_is_parameter}')
    def test(context, this_is_parameter):
        pass

I am trying to have all variables also in *quotes* - "".
Which serves the only purpose of having highlight in terminal.
Therefore knowing exactly which words of the decorator are variables on the first look.

.. note::

    There is only one self-defined exception - beggining of the decorator as can be seen with **m_btn**.
    This is not enforced. Just a best practise to keep close to the english sentences and not starting with a quote.


Explanation:

* **m_btn** - Mouse button - defined are **Left**, **Right** and **Middle**
* **name** - Any a11y defined name on the a11y object - <a11y_object>.name
* **role_name** - Any a11y defined roleName on the a11y object - <a11y_object>.roleName
* **attr** - Any a11y defined attribute on the a11y object - <a11y_object>.<attribute>
* **descr** - Any a11y defined description on the a11y object - <a11y_object>.description
* **root** - Any application defined in the a11y tree

.. note::

    Most used attributes are **showing**, **visible**, **checked**, **focused**, **sensitive**.

    Root object in case of decorators is the application defined in environment.py -> context.<*application*>.
    Use *application* string for **root** 



Mouse over 
----------

Decorators::

    Mouse over "{name}" "{role_name}"
    Mouse over "{name}" "{role_name}" in "{root}"
    Mouse over "{name}" "{role_name}" that is "{attr}"
    Mouse over "{name}" "{role_name}" that is "{attr}" in "{root}"
    Mouse over "{name}" "{role_name}" with description "{descr}"
    Mouse over "{name}" "{role_name}" with description "{descr}" in "{root}"
    Mouse over "{name}" "{role_name}" with description "{descr}" that is "{attr}"
    Mouse over "{name}" "{role_name}" with description "{descr}" that is "{attr}" in "{root}"

Explanation:

* **name** - Any a11y defined name on the a11y object - <a11y_object>.name
* **role_name** - Any a11y defined roleName on the a11y object - <a11y_object>.roleName
* **attr** - Any a11y defined attribute on the a11y object - <a11y_object>.<attribute>
* **descr** - Any a11y defined description on the a11y object - <a11y_object>.description
* **root** - Any application defined in the a11y tree

.. note::

    Most used attributes are **showing**, **visible**, **checked**, **focused**, **sensitive**.

    Root object in case of decorators is the application defined in environment.py -> context.<*application*>.
    Use *application* string for **root** 


Make an action 
--------------

Decorators::

    Make an action "{action}" for "{name}" "{role_name}"
    Make an action "{action}" for "{name}" "{role_name}" in "{root}"
    Make an action "{action}" for "{name}" "{role_name}" that is "{attr}"
    Make an action "{action}" for "{name}" "{role_name}" that is "{attr}" in "{root}"
    Make an action "{action}" for "{name}" "{role_name}" with description "{descr}"
    Make an action "{action}" for "{name}" "{role_name}" with description "{descr}" in "{root}"
    Make an action "{action}" for "{name}" "{role_name}" with description "{descr}" that is "{attr}"
    Make an action "{action}" for "{name}" "{role_name}" with description "{descr}" that is "{attr}" in "{root}"

Explanation:

* **action** - Any a11ly defined action on the object - <a11y_object>.actions.
* **name** - Any a11y defined name on the a11y object - <a11y_object>.name
* **role_name** - Any a11y defined roleName on the a11y object - <a11y_object>.roleName
* **attr** - Any a11y defined attribute on the a11y object - <a11y_object>.<attribute>
* **descr** - Any a11y defined description on the a11y object - <a11y_object>.description
* **root** - Any application defined in the a11y tree

.. note::

    Most used attributes are **showing**, **visible**, **checked**, **focused**, **sensitive**.

    Root object in case of decorators is the application defined in environment.py -> context.<*application*>.
    Use *application* string for **root** 




Item attribute positive
-----------------------

Decorators::

    Item "{name}" "{role_name}" found
    Item "{name}" "{role_name}" found in "{root}"
    Item "{name}" "{role_name}" is "{attr}"
    Item "{name}" "{role_name}" is "{attr}" in "{root}"
    Item "{name}" "{role_name}" with description "{descr}" is "{attr}"
    Item "{name}" "{role_name}" with description "{descr}" is "{attr}" in "{root}"

Explanation:

* **name** - Any a11y defined name on the a11y object - <a11y_object>.name
* **role_name** - Any a11y defined roleName on the a11y object - <a11y_object>.roleName
* **attr** - Any a11y defined attribute on the a11y object - <a11y_object>.<attribute>
* **descr** - Any a11y defined description on the a11y object - <a11y_object>.description
* **root** - Any application defined in the a11y tree

.. note::

    Most used attributes are **showing**, **visible**, **checked**, **focused**, **sensitive**.

    Root object in case of decorators is the application defined in environment.py -> context.<*application*>.
    Use *application* string for **root** 



Item attribute negative
-----------------------

Decorators::

    Item "{name}" "{role_name}" was not found
    Item "{name}" "{role_name}" was not found in "{root}"
    Item "{name}" "{role_name}" is not "{attr}"
    Item "{name}" "{role_name}" is not "{attr}" in "{root}"
    Item "{name}" "{role_name}" with description "{descr}" is not "{attr}"
    Item "{name}" "{role_name}" with description "{descr}" is not "{attr}" in "{root}"

Explanation:

* **name** - Any a11y defined name on the a11y object - <a11y_object>.name
* **role_name** - Any a11y defined roleName on the a11y object - <a11y_object>.roleName
* **attr** - Any a11y defined attribute on the a11y object - <a11y_object>.<attribute>
* **descr** - Any a11y defined description on the a11y object - <a11y_object>.description
* **root** - Any application defined in the a11y tree

.. note::

    Most used attributes are **showing**, **visible**, **checked**, **focused**, **sensitive**.

    Root object in case of decorators is the application defined in environment.py -> context.<*application*>.
    Use *application* string for **root** 



Item has text
-------------

Decorators::

    Item "{name}" "{role_name}" has text "{text}"
    Item "{name}" "{role_name}" has text "{text}" in "{root}"
    Item "{name}" "{role_name}" with description "{descr}" has text "{text}"
    Item "{name}" "{role_name}" with description "{descr}" has text "{text}" in "{root}"

Explanation:

* **name** - Any a11y defined name on the a11y object - <a11y_object>.name
* **role_name** - Any a11y defined roleName on the a11y object - <a11y_object>.roleName
* **text** - Any a11y defined name on the a11y object - <a11y_object>.text.
* **attr** - Any a11y defined attribute on the a11y object - <a11y_object>.<attribute>
* **descr** - Any a11y defined description on the a11y object - <a11y_object>.description
* **root** - Any application defined in the a11y tree

.. note::

    Most used attributes are **showing**, **visible**, **checked**, **focused**, **sensitive**.

    Root object in case of decorators is the application defined in environment.py -> context.<*application*>.
    Use *application* string for **root** 



Item does not have text
-----------------------

Decorators::
    
    Item "{name}" "{role_name}" does not have text "{text}"
    Item "{name}" "{role_name}" does not have text "{text}" in "{root}"
    Item "{name}" "{role_name}" with description "{descr}" does not have text "{text}"
    Item "{name}" "{role_name}" with description "{descr}" does not have text "{text}" in "{root}"

Explanation:

* **name** - Any a11y defined name on the a11y object - <a11y_object>.name
* **role_name** - Any a11y defined roleName on the a11y object - <a11y_object>.roleName
* **text** - Any a11y defined name on the a11y object - <a11y_object>.text.
* **attr** - Any a11y defined attribute on the a11y object - <a11y_object>.<attribute>
* **descr** - Any a11y defined description on the a11y object - <a11y_object>.description
* **root** - Any application defined in the a11y tree

.. note::

    Most used attributes are **showing**, **visible**, **checked**, **focused**, **sensitive**.

    Root object in case of decorators is the application defined in environment.py -> context.<*application*>.
    Use *application* string for **root** 



Item does not have desctiption
------------------------------

Decorators::

    Item "{name}" "{role_name}" does not have description "{descr}"
    Item "{name}" "{role_name}" does not have description "{descr}" in "{root}"
    Item "{name}" "{role_name}" does not have description "{descr}" that is "{attr}"
    Item "{name}" "{role_name}" does not have description "{descr}" that is "{attr}" in "{root}"

Explanation:

* **name** - Any a11y defined name on the a11y object - <a11y_object>.name
* **role_name** - Any a11y defined roleName on the a11y object - <a11y_object>.roleName
* **attr** - Any a11y defined attribute on the a11y object - <a11y_object>.<attribute>
* **descr** - Any a11y defined description on the a11y object - <a11y_object>.description
* **root** - Any application defined in the a11y tree

.. note::

    Most used attributes are **showing**, **visible**, **checked**, **focused**, **sensitive**.

    Root object in case of decorators is the application defined in environment.py -> context.<*application*>.
    Use *application* string for **root** 


Wait until the object changes state
-----------------------------------

Decorators::

    Wait until "{name}" "{role_name}" is "{attr}"
    Wait until "{name}" "{role_name}" is "{attr}" in "{root}"
    Wait until "{name}" "{role_name}" with description "{descr}" is "{attr}"
    Wait until "{name}" "{role_name}" with description "{descr}" is "{attr}" in "{root}"

Explanation:

* **name** - Any a11y defined name on the a11y object - <a11y_object>.name
* **role_name** - Any a11y defined roleName on the a11y object - <a11y_object>.roleName
* **attr** - Any a11y defined attribute on the a11y object - <a11y_object>.<attribute>
* **descr** - Any a11y defined description on the a11y object - <a11y_object>.description
* **root** - Any application defined in the a11y tree

.. note::

    Most used attributes are **showing**, **visible**, **checked**, **focused**, **sensitive**.

    Root object in case of decorators is the application defined in environment.py -> context.<*application*>.
    Use *application* string for **root** 



Wait until the object appears in a11y tree
------------------------------------------

Decorators::

    Wait until "{name}" "{role_name}" appears
    Wait until "{name}" "{role_name}" appears in "{root}"
    Wait until "{name}" "{role_name}" with description "{description}" appears
    Wait until "{name}" "{role_name}" with description "{description}" appears in "{root}"

Explanation:

* **name** - Any a11y defined name on the a11y object - <a11y_object>.name
* **role_name** - Any a11y defined roleName on the a11y object - <a11y_object>.roleName
* **attr** - Any a11y defined attribute on the a11y object - <a11y_object>.<attribute>
* **descr** - Any a11y defined description on the a11y object - <a11y_object>.description
* **root** - Any application defined in the a11y tree

.. note::

    Most used attributes are **showing**, **visible**, **checked**, **focused**, **sensitive**.

    Root object in case of decorators is the application defined in environment.py -> context.<*application*>.
    Use *application* string for **root** 




Start application
-----------------

Decorators::

    Start {application} via {start_via}
    Start "{application}" via command in {session}
    Start application "{application}" via "{start_via}"
    Start application "{application}" via command "{command}"
    Start application "{application}" via command in "{session}"
    Start application "{application}" via command "{command}" in "{session}"

Explanation:

* **application** - application defined in environment.py that is saved as context.<applicaiton>
* **start_via** - defined option are **menu** and **command**
* **command** - command to use when starting application
* **session** - specify if the command should be used in session - use **session** string in place of session variable.



Close application
-----------------

Decorators::

    Close app via gnome panel
    Close application "{application}" via "{close_via}"

Explanation:

* **application** - application defined in environment.py that is saved as context.<applicaiton>
* **close_via** - defined option are **gnome panel**, **application menu**, **shortcut** and **kill command**




Verify that application is no longer running
--------------------------------------------

Decorators::
    
    {application} shouldn\'t be running anymore
    Application "{application}" is no longer running

Explanation:

* **application** - application defined in environment.py that is saved as context.<applicaiton>




Verify that application is running
----------------------------------

Decorators::

    {application} should start
    Application "{application}" is running

Explanation:

* **application** - application defined in environment.py that is saved as context.<applicaiton>




Type text
---------

Decorators::

    Type text: "{text}"

Explanation:

* **text** - text to type




Press key
---------

Decorators::

    Press key: "{key_name}"

Explanation:

* **key_name** - key to press




Key combo
---------

Decorators::

    Press "{combo_name}"
    Key combo: "{combo_name}"

Explanation:

* **combo_name** - key combo to press




Wait few seconds before action
------------------------------

Decorators::

    Wait {number} second before action
    Wait {number} seconds before action

Explanation:

* **number** - integer of seconds to sleep




Move mouse to coordinates
-------------------------

Decorators::

    Move mouse to: x: "{position_x}", y: "{position_y}"

Explanation:

* **position_x** - integer of x coordinate
* **position_y** - integer of y coordinate




Mouse click to coordinates
--------------------------

Decorators::

    {button} click on: x: "{position_x}", y: "{position_y}"

Explanation:

* **m_btn** - Mouse button - defined are **Left**, **Right** and **Middle**
* **position_x** - integer of x coordinate
* **position_y** - integer of y coordinate

Using qecore as automation helper
*********************************

All parts of qecore
===================

.. toctree::
   :maxdepth: 3

   application
   common_steps_explain
   flatpak
   get_node
   image_matching
   logger
   online_accounts
   sandbox
   utility


History
=======
Before **qecore** pip module we used **behave-common-procedures** which was used as git submodule.

Qecore's base is behave-common-procedures so they are fully compatible.
But behave-common-procedures is frozen in its state, so as to not broke any tests that are still using it 
(only fixes will be backported). New features and code changes are instead done for the qecore.

.. note::
    
    Submodule behave-common-procedures was used, we tried to shorten the name when moving it to pip.
    
    And since we are Desktop **QE** team and used - behave-**co**\mmon-procedu\ **re**\s the **qecore** was created.


Installing beheve-common-procedures
===================================

In case the behave-common-procedures the clone be::

    git submodule add -b rhel-8 https://gitlab.cee.redhat.com/desktopqe/behave-common-procedures.git common_procedures --force

Note the changed name. This was the usage in the past with the submodule::

    >>> from common_procedures.sandbox import TestSandbox

.. note::
    
    This way is deprecated since it needs adjusted behave package build by our team. Qecore does not need it,
    as missing things are implemented on the qecore side.

Installing  qecore
==================

If system already has installed behave package - the recommendation is to remove them as qecore will install newest version::
    
    sudo dnf erase python3-behave

Installing with pip::

    sudo pip3 install qecore

Qecore has dependencies::

    behave # <- upstream behave pip module
    behave-html-formatter # <- pschindl's plugin to behave 
    pydbus # <- used for recording
    termcolor # <- used for nice error messages


To use behave's html formatter we need an **behave.ini** file with following content placed in $HOME folder or test project::

    # -- FILE: behave.ini
    # Define ALIAS for HtmlFormatter.
    [behave.formatters]
    html = behave_html_formatter:HTMLFormatter


Import then would be for example::

    >>> from qecore.sandbox import TestSandbox

Setup qecore
============

Setup of **environment.py**:

.. code-block:: python

    #!/usr/bin/env python3
    import sys
    import traceback
    from qecore.sandbox import TestSandbox


    def before_all(context):
        try:
            context.sandbox = TestSandbox("gnome-terminal")
            context.terminal = context.sandbox.get_application(name="gnome-terminal",
                                                               a11y_app_name="gnome-terminal-server")
        except Exception as error:
            print(f"Environment error: before_all: {error}")
            traceback.print_exc(file=sys.stdout)
            sys.exit(1)


    def before_scenario(context, scenario):
        try:
            context.sandbox.before_scenario(context, scenario)
        except Exception as error:
            print(f"Environment error: before_scenario: {error}")
            traceback.print_exc(file=sys.stdout)
            sys.exit(1)


    def after_scenario(context, scenario):
        try:
            context.sandbox.after_scenario(context, scenario)
        except Exception as error:
            print(f"Environment error: after_scenario: {error}")
            traceback.print_exc(file=sys.stdout)

Setup of **steps.py**:

.. code-block:: python

    #!/usr/bin/env python3
    from qecore.common_steps import *

Setup of **main.feature** - from this file all is controlled:

.. code-block:: gherkin

    Feature: Terminal test example

    @click_on_menu
    Scenario: Click on New Tab
        * Start application "terminal" via "command"
        * Left click "File" "menu"
        * Left click "New Tab" "menu item"

Setup of **runtest.sh**:

If you are automating for the DesktopQE and are expecting to have this project run via Jenkins.
You need to insert the following code to your project **runtest.sh** file.

.. code-block:: bash

    if [ ! -e /tmp/qecore_setup_done ]; then
        dnf -y erase python3-behave python2-behave # erase any behave installed by our install task
        sudo pip3 install qecore # install the qecore module
        touch /tmp/qecore_setup_done # do this setup only once
    fi

Since not everyone is using *qecore* we will do the install task "fix" in the runtest.
The best option would be having this directly in our install task but that would require everyone to use qecore.


Introduction
............

You will be activelly using only 2 modules (3 when you count common_steps - but that is just import - no setup) in most cases:
The :py:mod:`sandbox` and :py:mod:`application`.

The sandbox handles all system settings while application handles all application settings.

Sandbox
.......

Will initiate some values and has some settings done that can be changed any time by the user.

But for the general case nothing else, but initiation, is needed.

You can look at the :py:func:`sandbox.TestSandbox.__init__` source code to see what values are set.
You can change any value to change the bahaviour of :py:func:`sandbox.TestSandbox.before_scenario`
and :py:func:`sandbox.TestSandbox.after_scenario` 

    .. note::

        You want to save the context.sandbox so that you can access it through all files assocciated with behave.
    
    .. note::

        After __init__ is done, the context.sandbox.shell has a11y object of gnome-shell and can be used to automate the system.


Application
...........

Will initiate some values and has some settings done that can be changed any time by the user.

But for the general case nothing else, but initiation, is needed.

You can look at the :py:func:`application.Application.__init__` source code to see what values are set.

This is called through the :py:func:`sandbox.TestSandbox.get_application` and does not need to be called on your own.
This is done so that we can keep in mind what is default application, what is opened, what needs to be closed after the test.

    .. note::

        You want to save the context.<app> so that you can access it through all files assocciated with behave.

    .. note::

        After __init__ is done, the context.<app>.instance has a11y object of the <app> and is used to automate the application.

Common steps
............

Pre-coded behave steps designed to be usable from feature files and not require coding skills.

For more information about the usage see :ref:`common_steps_explain`

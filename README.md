## QECORE

A sandbox of sorts for test execution. Paired with behave and dogtail this project serves as a useful tool for test execution with minimal required setup.

[Project Documentation in gitlab Pages](https://dogtail.gitlab.io/qecore/index.html) - build by CI pipelines on every change
